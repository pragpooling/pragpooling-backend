# PragPooling App

### Running the app
Open the terminal on the root folder of the app and run the following command: `mvn org.springframework.boot:spring-boot-maven-plugin:run
`

### Creating a user

On any RESTful webservice client, like postman, execute a **POST** request with the url `http://localhost:8080/api/v1/user` and the next body:
````
{
    "name": "daniel",
    "lastname": "ramirez",
    "telephoneNumber": "321",
    "address": "calle123",
    "email": "abc@correo.com",
    "password": "Contra1*"
}
````
All the previous fields are required. The password must be between 8 and 15 characters. It must contain at least an uppercase, a lowercase, a number, and any of the following characters `*_-`.

### Finding a user

Execute a **GET** request with `email` as a query param like this: `http://localhost:8080/api/v1/user?email=abc@correo.com`.
It will return a response with all of the user's information without the password:

````
{
    "name": "daniel",
    "lastname": "ramirez",
    "telephoneNumber": "321",
    "address": "calle123",
    "email": "abc@correo.com"
}
````

# Tech Specs

- Springboot
- RESTful web services
- Spring data jpa
- H2 Database
- Flyway
- Lombok
- Junit 5
- Mockito
- Mockmvc
- Regex
- Javax validations
- Custom exception handler