package com.pragma.pragpooling.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.pragma.pragpooling.dtos.UserRequestDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTest {

  @Autowired
  private ObjectMapper objectMapper;

  @Autowired
  private MockMvc mockMvc;

  private final String URL = "/api/v1/user";

  @Test
  public void createUser_OkTest() throws Exception {
    UserRequestDto request = createUserRequestDtoMock();
    ObjectNode objectNode = objectMapper.valueToTree(request);
    objectNode.put("password", request.getPassword());

    mockMvc
        .perform(MockMvcRequestBuilders.post(URL)
            .content(objectNode.toString())
            .contentType(MediaType.APPLICATION_JSON)
        )
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andReturn();
  }

  @Test
  public void createUser_BadRequestTest() throws Exception {
    UserRequestDto badRequest = createUserRequestDtoMock()
        .toBuilder()
        .password("invalidpassword")
        .email("invalidemail")
        .build();
    mockMvc
        .perform(MockMvcRequestBuilders.post(URL)
            .content(objectMapper.writeValueAsString(badRequest))
            .contentType(MediaType.APPLICATION_JSON)
        )
        .andExpect(MockMvcResultMatchers.status().isBadRequest())
        .andReturn();
  }

  @Test
  @Sql(
      executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
      scripts = {
          "classpath:sql/clean_user_table.sql",
          "classpath:sql/insert_user.sql"
      }
  )
  public void getUserByEmail_OkTest() throws Exception {
    String actualResponse = mockMvc
        .perform(MockMvcRequestBuilders.get(URL)
            .queryParam("email", "daniel@correo.com")
            .contentType(MediaType.APPLICATION_JSON)
        )
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andReturn()
        .getResponse().getContentAsString();

    UserRequestDto expectedResponse = createUserRequestDtoMock();

    Assertions.assertEquals(objectMapper.writeValueAsString(expectedResponse), actualResponse);

  }

  @Test
  public void getUserByEmail_NotFoundTest() throws Exception {
    mockMvc
        .perform(MockMvcRequestBuilders.get(URL)
            .queryParam("email", "usuarioinexistente@correo.com")
            .contentType(MediaType.APPLICATION_JSON)
        )
        .andExpect(MockMvcResultMatchers.status().isNotFound())
        .andReturn();
  }

  private UserRequestDto createUserRequestDtoMock() {
    return UserRequestDto.builder()
        .name("daniel")
        .lastname("ramirez")
        .telephoneNumber("321")
        .address("calle 123")
        .email("daniel@correo.com")
        .password("Contra1*")
        .build();
  }

}
