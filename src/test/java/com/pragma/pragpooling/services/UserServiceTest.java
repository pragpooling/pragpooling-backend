package com.pragma.pragpooling.services;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.pragma.pragpooling.models.User;
import com.pragma.pragpooling.repositories.UserRepository;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

  @Mock
  private UserRepository userRepository;

  @InjectMocks
  private UserService userService;

  @Test
  public void saveUserTest() {
    User userMock = createUserMock();
    when(userRepository.save(any()))
        .thenReturn(userMock);
    User userSaved = userService.saveUser(userMock);
    assertNotNull(userSaved);
  }

  @Test
  public void findUserByEmailTest() {
    User userMock = createUserMock();
    when(userRepository.findByEmail(anyString()))
        .thenReturn(Optional.of(userMock));
    Optional<User> user = userService.findUserByEmail("correo@correo.com");
    assertTrue(user.isPresent());
  }

  private User createUserMock() {
    return User.builder()
        .name("")
        .lastname("")
        .telephoneNumber("")
        .address("")
        .email("")
        .password("")
        .build();
  }
}
