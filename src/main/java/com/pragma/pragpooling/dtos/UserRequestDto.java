package com.pragma.pragpooling.dtos;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.pragma.pragpooling.utils.RegexUtil;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class UserRequestDto {

  @NotEmpty(message = "El nombre no debe estar vacío")
  private String name;

  @NotEmpty(message = "El apellido no debe estar vacío")
  private String lastname;

  @NotEmpty(message = "El número telefónico no debe estar vacío")
  private String telephoneNumber;

  @NotEmpty(message = "La dirección no debe estar vacía")
  private String address;

  @NotEmpty(message = "El correo no debe estar vacío")
  @Email(message = "Se debe utilizar una dirección de correo válida")
  private String email;

  //Avoid password to get serialized, but allows to be deserialized.
  @Getter(onMethod = @__(@JsonIgnore))
  @Setter(onMethod = @__(@JsonProperty))
  @NotEmpty(message = "La contraseña no debe estar vacía")
  @Pattern(regexp = RegexUtil.PASSWORD_REGEX, message = "La contraseña debe cumplir lo siguiente: "
      + "longitud mínima de 8 caracteres y máxima de 15, debe contener letras mayúsculas minusculas, "
      + "numeros y un carácter como *_-")
  private String password;

}
