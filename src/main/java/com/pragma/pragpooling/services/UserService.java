package com.pragma.pragpooling.services;

import com.pragma.pragpooling.models.User;
import com.pragma.pragpooling.repositories.UserRepository;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserService {

  private final UserRepository userRepository;

  public User saveUser(User user) {
    log.info("Persistiendo usuario {} {} en la base de datos", user.getName(), user.getLastname());
    return userRepository.save(user);
  }

  public Optional<User> findUserByEmail(String email) {
    log.info("Consultando usuario identificado por el correo {}", email);
    return userRepository.findByEmail(email);
  }

}
