package com.pragma.pragpooling.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class RegexUtil {

  private static final String MINIMO_UNA_MAYUSCULA = "(?=(.*[A-Z])+)";
  private static final String MINIMO_UNA_MINUSCULA = "(?=(.*[a-z])+)";
  private static final String MINIMO_UN_NUMERO = "(?=(.*[0-9])+)";
  private static final String MINIMO_UN_CARACTER_ESPECIAL = "(?=(.*[*_-])+)";
  private static final String LONGITUD_ENTRE_8_Y_15_CARACTERES = ".{8,15}";

  public static final String PASSWORD_REGEX = "^"
      + MINIMO_UNA_MAYUSCULA
      + MINIMO_UNA_MINUSCULA
      + MINIMO_UN_NUMERO
      + MINIMO_UN_CARACTER_ESPECIAL
      + LONGITUD_ENTRE_8_Y_15_CARACTERES
      + "$";

}
