package com.pragma.pragpooling.mappers;

import com.pragma.pragpooling.dtos.UserRequestDto;
import com.pragma.pragpooling.models.User;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class UserMapper {

  public static User mapUserRequestDtoToUser(UserRequestDto userRequestDto) {
    return User.builder()
        .name(userRequestDto.getName())
        .lastname(userRequestDto.getLastname())
        .telephoneNumber(userRequestDto.getTelephoneNumber())
        .address(userRequestDto.getAddress())
        .email(userRequestDto.getEmail())
        .password(userRequestDto.getPassword())
        .build();
  }

  public static UserRequestDto mapUserToUserRequestDto(User user) {
    return UserRequestDto.builder()
        .name(user.getName())
        .lastname(user.getLastname())
        .telephoneNumber(user.getTelephoneNumber())
        .address(user.getAddress())
        .email(user.getEmail())
        .password(user.getPassword())
        .build();
  }
}
