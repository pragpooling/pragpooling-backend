package com.pragma.pragpooling.controllers;

import com.pragma.pragpooling.dtos.UserRequestDto;
import com.pragma.pragpooling.mappers.UserMapper;
import com.pragma.pragpooling.models.User;
import com.pragma.pragpooling.services.UserService;
import java.util.Optional;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/user")
@RequiredArgsConstructor
@Slf4j
public class UserController {

  private final UserService userService;

  @PostMapping
  public ResponseEntity<String> createUser(@Valid @RequestBody UserRequestDto userRequestDto) {
    log.info("Inicia creación de usuario");
    log.info("Request recibido: " + userRequestDto);
    User user = userService.saveUser(UserMapper.mapUserRequestDtoToUser(userRequestDto));
    log.info("Se creó el usuario {} {} en la base de datos", user.getName(), user.getLastname());
    log.info("Finaliza creación de usuario");
    return ResponseEntity.ok("Usuario creado exitosamente");
  }

  @GetMapping
  public ResponseEntity<UserRequestDto> getUserByEmail(@RequestParam String email) {
    log.info("Inicia consulta de usuario");
    Optional<UserRequestDto> userRequestDto = userService.findUserByEmail(email)
        .map(UserMapper::mapUserToUserRequestDto);
    log.info("Finaliza consulta de usuario");
    return ResponseEntity.of(userRequestDto);
  }
}
